import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengantarDetailPage } from './pengantar-detail.page';

describe('PengantarDetailPage', () => {
  let component: PengantarDetailPage;
  let fixture: ComponentFixture<PengantarDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengantarDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengantarDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
