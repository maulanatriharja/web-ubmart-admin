import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-pengantar-detail',
  templateUrl: './pengantar-detail.page.html',
  styleUrls: ['./pengantar-detail.page.scss'],
})
export class PengantarDetailPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  tutup() {
    this.modalController.dismiss();
  }

}
