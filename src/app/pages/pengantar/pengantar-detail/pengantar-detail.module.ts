import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PengantarDetailPageRoutingModule } from './pengantar-detail-routing.module';

import { PengantarDetailPage } from './pengantar-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PengantarDetailPageRoutingModule
  ],
  declarations: [PengantarDetailPage]
})
export class PengantarDetailPageModule {}
