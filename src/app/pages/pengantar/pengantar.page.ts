import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PengantarDetailPage } from './pengantar-detail/pengantar-detail.page';

@Component({
  selector: 'app-pengantar',
  templateUrl: './pengantar.page.html',
  styleUrls: [ './pengantar.page.scss' ],
})
export class PengantarPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async detail() {
    const modal = await this.modalController.create({
      component: PengantarDetailPage,
      cssClass: 'custom-modal'
    });
    return await modal.present();
  }

}
