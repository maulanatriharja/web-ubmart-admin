import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DesaDetailPageRoutingModule } from './desa-detail-routing.module';

import { DesaDetailPage } from './desa-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DesaDetailPageRoutingModule
  ],
  declarations: [DesaDetailPage]
})
export class DesaDetailPageModule {}
