import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-desa-detail',
  templateUrl: './desa-detail.page.html',
  styleUrls: ['./desa-detail.page.scss'],
})
export class DesaDetailPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  tutup() {
    this.modalController.dismiss();
  }

}
