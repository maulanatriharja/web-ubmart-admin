import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DesaDetailPage } from './desa-detail.page';

describe('DesaDetailPage', () => {
  let component: DesaDetailPage;
  let fixture: ComponentFixture<DesaDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesaDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DesaDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
