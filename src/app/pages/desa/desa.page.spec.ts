import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DesaPage } from './desa.page';

describe('DesaPage', () => {
  let component: DesaPage;
  let fixture: ComponentFixture<DesaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DesaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
