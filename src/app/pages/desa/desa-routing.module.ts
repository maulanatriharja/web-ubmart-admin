import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DesaPage } from './desa.page';

const routes: Routes = [
  {
    path: '',
    component: DesaPage
  },
  {
    path: 'desa-detail',
    loadChildren: () => import('./desa-detail/desa-detail.module').then( m => m.DesaDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DesaPageRoutingModule {}
