import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DesaDetailPage } from './desa-detail/desa-detail.page';

@Component({
  selector: 'app-desa',
  templateUrl: './desa.page.html',
  styleUrls: [ './desa.page.scss' ],
})
export class DesaPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async detail() {
    const modal = await this.modalController.create({
      component: DesaDetailPage,
      cssClass: 'custom-modal'
    });
    return await modal.present();
  }

}
