import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DesaPageRoutingModule } from './desa-routing.module';

import { DesaPage } from './desa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DesaPageRoutingModule
  ],
  declarations: [DesaPage]
})
export class DesaPageModule {}
