import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PelangganPage } from './pelanggan.page';

const routes: Routes = [
  {
    path: '',
    component: PelangganPage
  },
  {
    path: 'pelanggan-detail',
    loadChildren: () => import('./pelanggan-detail/pelanggan-detail.module').then( m => m.PelangganDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PelangganPageRoutingModule {}
