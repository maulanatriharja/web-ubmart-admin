import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PelangganPage } from './pelanggan.page';

describe('PelangganPage', () => {
  let component: PelangganPage;
  let fixture: ComponentFixture<PelangganPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelangganPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PelangganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
