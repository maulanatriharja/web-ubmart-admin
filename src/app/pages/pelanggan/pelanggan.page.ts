import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PelangganDetailPage } from './pelanggan-detail/pelanggan-detail.page';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: [ './pelanggan.page.scss' ],
})
export class PelangganPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async detail() {
    const modal = await this.modalController.create({
      component: PelangganDetailPage,
      cssClass: 'custom-modal'
    });
    return await modal.present();
  }

}
