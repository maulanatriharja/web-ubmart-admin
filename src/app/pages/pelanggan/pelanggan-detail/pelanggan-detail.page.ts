import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-pelanggan-detail',
  templateUrl: './pelanggan-detail.page.html',
  styleUrls: ['./pelanggan-detail.page.scss'],
})
export class PelangganDetailPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  tutup() {
    this.modalController.dismiss();
  }

}
