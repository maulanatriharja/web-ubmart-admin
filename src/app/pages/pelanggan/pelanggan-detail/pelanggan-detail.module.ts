import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PelangganDetailPageRoutingModule } from './pelanggan-detail-routing.module';

import { PelangganDetailPage } from './pelanggan-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PelangganDetailPageRoutingModule
  ],
  declarations: [PelangganDetailPage]
})
export class PelangganDetailPageModule {}
