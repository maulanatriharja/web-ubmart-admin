import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PelangganDetailPage } from './pelanggan-detail.page';

describe('PelangganDetailPage', () => {
  let component: PelangganDetailPage;
  let fixture: ComponentFixture<PelangganDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelangganDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PelangganDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
