import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaerahDetailPageRoutingModule } from './daerah-detail-routing.module';

import { DaerahDetailPage } from './daerah-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaerahDetailPageRoutingModule
  ],
  declarations: [DaerahDetailPage]
})
export class DaerahDetailPageModule {}
