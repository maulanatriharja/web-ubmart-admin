import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-daerah-detail',
  templateUrl: './daerah-detail.page.html',
  styleUrls: [ './daerah-detail.page.scss' ],
})
export class DaerahDetailPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  tutup() {
    this.modalController.dismiss();
  }

}
