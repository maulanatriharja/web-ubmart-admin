import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaerahDetailPage } from './daerah-detail.page';

const routes: Routes = [
  {
    path: '',
    component: DaerahDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaerahDetailPageRoutingModule {}
