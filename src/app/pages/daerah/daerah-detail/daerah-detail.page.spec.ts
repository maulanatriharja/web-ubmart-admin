import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaerahDetailPage } from './daerah-detail.page';

describe('DaerahDetailPage', () => {
  let component: DaerahDetailPage;
  let fixture: ComponentFixture<DaerahDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaerahDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaerahDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
