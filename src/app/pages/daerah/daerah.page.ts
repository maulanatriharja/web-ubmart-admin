import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DaerahDetailPage } from './daerah-detail/daerah-detail.page';

@Component({
  selector: 'app-daerah',
  templateUrl: './daerah.page.html',
  styleUrls: [ './daerah.page.scss' ],
})
export class DaerahPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async detail() {
    const modal = await this.modalController.create({
      component: DaerahDetailPage,
      cssClass: 'custom-modal'
    });
    return await modal.present();
  }

}
