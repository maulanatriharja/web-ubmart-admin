import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaerahPage } from './daerah.page';

const routes: Routes = [
  {
    path: '',
    component: DaerahPage
  },
  {
    path: 'daerah-detail',
    loadChildren: () => import('./daerah-detail/daerah-detail.module').then( m => m.DaerahDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaerahPageRoutingModule {}
