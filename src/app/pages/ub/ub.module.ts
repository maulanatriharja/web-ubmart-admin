import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UbPageRoutingModule } from './ub-routing.module';

import { UbPage } from './ub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbPageRoutingModule
  ],
  declarations: [UbPage]
})
export class UbPageModule {}
