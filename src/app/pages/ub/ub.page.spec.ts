import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UbPage } from './ub.page';

describe('UbPage', () => {
  let component: UbPage;
  let fixture: ComponentFixture<UbPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UbPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
