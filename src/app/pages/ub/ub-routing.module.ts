import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UbPage } from './ub.page';

const routes: Routes = [
  {
    path: '',
    component: UbPage
  },
  {
    path: 'ub-detail',
    loadChildren: () => import('./ub-detail/ub-detail.module').then( m => m.UbDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbPageRoutingModule {}
