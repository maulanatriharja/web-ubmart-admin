import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ub-detail',
  templateUrl: './ub-detail.page.html',
  styleUrls: ['./ub-detail.page.scss'],
})
export class UbDetailPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  tutup() {
    this.modalController.dismiss();
  }

}
