import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UbDetailPage } from './ub-detail.page';

const routes: Routes = [
  {
    path: '',
    component: UbDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbDetailPageRoutingModule {}
