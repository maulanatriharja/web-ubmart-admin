import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UbDetailPageRoutingModule } from './ub-detail-routing.module';

import { UbDetailPage } from './ub-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbDetailPageRoutingModule
  ],
  declarations: [UbDetailPage]
})
export class UbDetailPageModule {}
