import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UbDetailPage } from './ub-detail.page';

describe('UbDetailPage', () => {
  let component: UbDetailPage;
  let fixture: ComponentFixture<UbDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UbDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
