import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UbDetailPage } from './ub-detail/ub-detail.page';

@Component({
  selector: 'app-ub',
  templateUrl: './ub.page.html',
  styleUrls: [ './ub.page.scss' ],
})
export class UbPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async detail() {
    const modal = await this.modalController.create({
      component: UbDetailPage,
      cssClass: 'custom-modal'
    });
    return await modal.present();
  }

}
