import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [ 'app.component.scss' ]
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public menu_main = [
    {
      title: 'Dashboard',
      url: 'dashboard',
      icon: 'speedometer'
    },{
      title: 'Data UB',
      url: 'ub',
      icon: 'home'
    }, {
      title: 'Data Pengantar',
      url: 'pengantar',
      icon: 'bicycle'
    }, {
      title: 'Data Pelanggan',
      url: 'pelanggan',
      icon: 'people-circle'
    }
  ];

  public menu_location = [
    {
      title: 'Data Daerah',
      url: 'daerah',
      icon: 'grid'
    }, {
      title: 'Data Desa',
      url: 'desa',
      icon: 'apps'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[ 1 ];
    if (path !== undefined) {
      this.selectedIndex = this.menu_main.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
