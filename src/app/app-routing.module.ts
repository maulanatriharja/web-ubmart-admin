import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'ub',
    loadChildren: () => import('./pages/ub/ub.module').then(m => m.UbPageModule)
  },
  {
    path: 'pengantar',
    loadChildren: () => import('./pages/pengantar/pengantar.module').then(m => m.PengantarPageModule)
  },
  {
    path: 'pelanggan',
    loadChildren: () => import('./pages/pelanggan/pelanggan.module').then(m => m.PelangganPageModule)
  },
  {
    path: 'daerah',
    loadChildren: () => import('./pages/daerah/daerah.module').then(m => m.DaerahPageModule)
  },
  {
    path: 'desa',
    loadChildren: () => import('./pages/desa/desa.module').then(m => m.DesaPageModule)
  },
  {
    path: 'transaksi',
    loadChildren: () => import('./pages/transaksi/transaksi.module').then(m => m.TransaksiPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
